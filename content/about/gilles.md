---
draft: false
lastName: Testard
mobile: +336 66 09 61 09
date: 2020-12-12T16:24:16+01:00
people: true
title: Gilles Testard
image: images/gilles-testard.jpg
firstName: Gilles
job: Responsable FreeLance
group: null
---
Je suis ici pour vous proposer des solutions informatiques basées sur le Web. \
Elle vous permettront de présenter efficacement vos centres d'intérêt : métier, entreprise, association, loisirs.

J'aime que ce que je "fabrique" soit utile!
J'aime apprendre,  cela tombe bien! dans ce domaine professionnel c'est indispensable.

Paradoxalement, je me sens **Geek**, toujours à la recherche de solutions webs, pratiques et efficaces **mais mon autre grand passion** est le sport canin, au travers des **Retrievers** et des compétions de Working Test. 
J'aime à basculer du monde numérique au monde de la nature...

Ma plus profonde motivation est et sera toujours ma famille : ma femme, mes filles et petits enfants!