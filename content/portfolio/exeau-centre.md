---
title: "Exeau Centre"
date: 2020-12-09T16:09:58+01:00
id: "b75d655ad058d380cd1145887479a1b9"
draft: false
visible: true
image: images/sites/exeau-main.jpg
job: Travaux Public
tags: 
- Joomla
- Site Vitrine
domaineName: coiffeurdebienetre
extension: fr
framework: Joomla
graphisme: original
js: jQuery
css: Css3
html: html5
responsif: Responsif
equipt:
- loader
- contact par mail et adresse
- recommander à des relations
- fiche détail
- demande de rendez vous sur article ciblé
- appel téléphone direct
- proposer son bien à la vente
- moteur de recherche
- recommendations sociales
- horaires ouverture et fermeture
- affichage honoraires
- présentation de l'équipe
- mentions légales
- RGPD
- plan du site
- admin personnalisée
composants:
- RsForm
- Form2Content
- ImageRecycle
- Regular Labs Snippets
- PWT ACL
- Speed Cache
- AdminTools
- AkeebaBackup
- JSitemap
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum in nunc sit amet euismod. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc tempor dui in efficitur aliquet. Ut sit amet condimentum augue. Phasellus quis lectus mollis, tempor nibh eget, volutpat augue. Vestibulum in egestas erat. Donec sit amet hendrerit felis. Fusce a pretium nulla, venenatis commodo purus. Nullam sed eleifend enim. Nam porta condimentum purus nec interdum. Aliquam congue in erat ut dignissim.

Sed ullamcorper elementum feugiat. Maecenas aliquet erat eget nunc ultrices mattis. Mauris mattis diam at est gravida suscipit. Aenean in neque lacinia, tincidunt urna quis, fermentum metus. Phasellus ac congue nulla, at convallis lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi ac felis non lectus lacinia fringilla nec a tortor. Maecenas rhoncus malesuada placerat. Mauris neque lorem, varius eget congue eget, luctus a libero. Praesent maximus commodo ante. Ut turpis sem, viverra id orci et, dapibus sollicitudin nunc.

Nulla quis turpis ex. Fusce et vehicula lectus. Aliquam tristique justo ante, sit amet congue est hendrerit id. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam nec leo accumsan, sagittis nulla quis, sodales lacus. Pellentesque vulputate diam quis convallis scelerisque. Phasellus porta fermentum ante, sit amet pulvinar mauris pretium id. Proin tempus neque vulputate, viverra nisl a, porta justo. Aenean pellentesque, tellus id tincidunt elementum, quam arcu semper sapien, ut bibendum arcu nibh a nulla. Suspendisse dolor ex, maximus in rutrum non, congue quis libero. Sed eu dignissim metus. Cras maximus metus et sagittis bibendum. Phasellus a rhoncus tortor, non vehicula augue. Aliquam elit nisl, congue a dolor id, egestas sagittis nulla. Vivamus tristique euismod mi sed sagittis. 