---
title: "Amis De Diane"
date: 2020-12-09T15:40:27+01:00
id: "59f7101460c1c11784edd0e55977974f"
draft: false
visible: true
image: images/sites/amis-de-diane-main.jpg
job: Hôtel Restaurant
tags: 
- Joomla
- Site Vitrine
domaineName: restaurant-amisdediane
extension: com
framework: Joomla
graphisme: original
js: jQuery
css: Css3
html: html5
responsif: Responsif
equipt:
- contact par mail et adresse
- appel téléphone direct
- recommendations sociales
- horaires ouverture et fermeture
- affichage carte du restaurant
- Googe Map
- mentions légales
- RGPD
- plan du site
- admin personnalisée
composants:
- RsForm
- Form2Content
- ImageRecycle
- Regular Labs Snippets
- PWT ACL
- AdminTools
- AkeebaBackup
- JSitemap
---



Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum in nunc sit amet euismod. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc tempor dui in efficitur aliquet. Ut sit amet condimentum augue. Phasellus quis lectus mollis, tempor nibh eget, volutpat augue. Vestibulum in egestas erat. Donec sit amet hendrerit felis. Fusce a pretium nulla, venenatis commodo purus. Nullam sed eleifend enim. Nam porta condimentum purus nec interdum. Aliquam congue in erat ut dignissim.

Sed ullamcorper elementum feugiat. Maecenas aliquet erat eget nunc ultrices mattis. Mauris mattis diam at est gravida suscipit. Aenean in neque lacinia, tincidunt urna quis, fermentum metus. Phasellus ac congue nulla, at convallis lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi ac felis non lectus lacinia fringilla nec a tortor. Maecenas rhoncus malesuada placerat. Mauris neque lorem, varius eget congue eget, luctus a libero. Praesent maximus commodo ante. Ut turpis sem, viverra id orci et, dapibus sollicitudin nunc.

Nulla quis turpis ex. Fusce et vehicula lectus. Aliquam tristique justo ante, sit amet congue est hendrerit id. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam nec leo accumsan, sagittis nulla quis, sodales lacus. Pellentesque vulputate diam quis convallis scelerisque. Phasellus porta fermentum ante, sit amet pulvinar mauris pretium id. Proin tempus neque vulputate, viverra nisl a, porta justo. Aenean pellentesque, tellus id tincidunt elementum, quam arcu semper sapien, ut bibendum arcu nibh a nulla. Suspendisse dolor ex, maximus in rutrum non, congue quis libero. Sed eu dignissim metus. Cras maximus metus et sagittis bibendum. Phasellus a rhoncus tortor, non vehicula augue. Aliquam elit nisl, congue a dolor id, egestas sagittis nulla. Vivamus tristique euismod mi sed sagittis. 