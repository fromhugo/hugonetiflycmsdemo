---
draft: false
css: Css3
equipt:
  - contact par mail et adresse
  - recommendations sociales
  - horaires ouverture et fermeture
  - affichage honoraires
  - présentation de l'équipe
  - mentions légales
  - RGPD
  - plan du site
js: jQuery
html: html5
responsif: Responsif
domaineName: coiffeurdebienetre
date: 2020-12-08T15:13:03+01:00
graphisme: original
title: Coiffeur De Bien Etre
image: images/sites/cbe-main.jpg
job: Coiffeur
tags:
  - Click and Collect
  - Joomla
  - Site Vitrine
visible: true
framework: Joomla
extension: fr
composants:
  - RsForm
  - Form2Content
  - ImageRecycle
  - Regular Labs Snippets
  - PWT ACL
  - J2Store
  - AdminTools
  - AkeebaBackup
  - JSitemap
---

Coiffeur de bien être est un site vitrine dédié à la présentation des deux salons de coiffure : "Coiffeur de bien être", l'un à Châteauneuf sur Loire, l'autre à Sully sur Loire.

Chaque mois, il lui est ajouté quelques articles de news destinés à entretenir le lien avec la clientèle et entretenir le référencement naturel.

Le facteur humain est systématiquement mis en avant par la présentation de l'arrivée et l'accueil des apprentis.

Une page est dédiée à chacun des salons.
Elle présente le salon, le personnel et la localisation.
De cette page un mail peut être adressé.

Sur toutes les pages du site, les téléphones sont rappelés et activables.
Un bouton de connexion à la plateforme de prise de rendez vous y est tout autant présent.

Le site a été équipé d'une boutique "Click &amp; Collect".
Ce Click &amp; Collect propose tous les produits proposés par les salons "physiques" : bains, soins, texturisants