---
draft: false
css: Css3
equipt:
  - loader
  - contact par mail et adresse
  - recommander à des relations
  - fiche détail
  - demande de rendez vous sur article ciblé
  - appel téléphone direct
  - proposer son bien à la vente
  - moteur de recherche
  - recommendations sociales
  - horaires ouverture et fermeture
  - affichage honoraires
  - présentation de l'équipe
  - mentions légales
  - RGPD
  - plan du site
  - admin personnalisée
js: jQuery
html: html5
responsif: Responsif
domaineName: immobiliere-des-lilas
date: 2020-12-07T11:49:50+01:00
graphisme: original
title: Agence immobilière des Lilas
image: images/sites/lilas-main.jpg
job: Immobilier
tags:
  - Joomla
  - Site Vitrine
visible: true
framework: Joomla
extension: immo
composants:
  - Joomla Estate Agency
  - RsForm
  - Form2Content
  - ImageRecycle
  - Regular Labs Snippets
  - PWT ACL
  - Speed Cache
  - AdminTools
  - AkeebaBackup
  - JSitemap
---

Le site de l'Agence immobilière des Lilas est un site vitrine dédié à la présentation de biens à louer ou à vendre.

Les biens sont présentés, résumés, sur page de catégorie (Louer, Acheter).
Sur cette page, une photo avec bandeau épinglé...

Il permet le présentation d'une fiche technique élaborée pour chacun des biens : surface habitable, composition, agencement, nombre de pièces, prix et honoraires etc

Ce site, utilise un nombre important de photos.
Cela ralentit un peu sa présentation, c'est pourquoi, il est doté d'un "loader" : animation légère destinée à rassurer le visiteur pour le faire patienter.
Ceci dit, c'est une attente légère.
Les photos sont redimensionnées à l'upload. Une application : ImageRecycle parcours l'ensemble des médias, chaque 24h, pour compresser les fichiers qui seraient trop lourds.