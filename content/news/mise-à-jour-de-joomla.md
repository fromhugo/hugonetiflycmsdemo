---
title: Mise à jour de Joomla!
date: 2021-01-14T09:51:11.797Z
draft: false
image: /images/horizontal-joomla-logo-light.jpg
tags:
  - joomla
keywords:
  - website
  - dev
visible: true
---
Mise à jour des sites de mon parc développés sur le CMS Joomla

En effet, mail de Joomla Security News qui demande la mise à jour des Cms Joomla à la version 3.9.24
Donc c'est fait, profitant de l'occasion je mets à jour les différents composant de parefeu, backup, mail etc... qui en ont besoin.