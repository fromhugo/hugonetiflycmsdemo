---
title: Bienvenue
draft: false
keywords:
  - website
author: null
date: 2020-11-30T12:03:29+01:00
imgGallery: null
image: /images/img_0078.jpg
tags:
  - news
visible: true
---
Hello tout le monde !
Je débute un nouveau site pour ma micro entreprise de développement Web.
En effet depuis la création du précédent : galites.net, bien des choses ont changées.
En tous cas, maintenant nous nous retrouverons sur webensol.site 

".site" est l'extension! et oui, cela existe maintenant.
Sinon Web en Sol comme do, ré, mi, fa, sol (je travaille toujours en musique, donc !), mais aussi Sol comme Sologne, la région où je vis, voilà pour la petite histoire!

Maintenant, tout à changer, non seulement la société, mais tout le monde le sait, les techniques informatiques bougent à la vitesses grand V.
Donc je peux maintenant en proposer plus.
Avant la label : Site statique sonnait péjoratif, maintenant c'est une technique moderne et efficace tant au niveau des performances qu'au niveau de la sécurité.
D'ailleurs, WebEnSol est un static webiste.
Les sites internet peuvent se comporter en Application mobiles au travers le PWA ...
Des solutions Ecommerce légères et dont l'intégration est sortie de l'aire "usine à gaz" existent maintenant.

Donc voilà un court résumé du pourquoi du comment de la chose!

Je vous souhaite à tous une année 2021 qui soit la plus confortable possible avec une sortie de la problématique COVID rapide et efficace.
Gilles 
