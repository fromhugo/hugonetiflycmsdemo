const loader = document.getElementById('loader')
function delay(n) {
    return new Promise((done) => {
        setTimeout(() => {
            done();
        }, n)
    })
}
barba.init({
    sync: true,
    transitions: [
        {
            async leave() {
                const done = this.async();
                loader.classList.add('is-there')
                await delay(1500);
                done();
            },
            enter() {
                loader.classList.remove('is-there')
            }
        }
    ] 
})