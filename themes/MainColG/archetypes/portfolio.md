---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
id: "{{ .File.UniqueID }}"
draft: true
visible: true
image: 
job:
tags: 
- Joomla
- Site Vitrine
- Coiffure
domaineName: coiffeurdebienetre
extension: fr
framework: Joomla
graphisme: original
js: jQuery
css: Css3
html: html5
responsif: Responsif
equipt:
- loader
- contact par mail et adresse
- recommander à des relations
- fiche détail
- demande de rendez vous sur article ciblé
- appel téléphone direct
- proposer son bien à la vente
- moteur de recherche
- recommendations sociales
- horaires ouverture et fermeture
- affichage honoraires
- présentation de l'équipe
- mentions légales
- RGPD
- plan du site
- admin personnalisée
composants:
- RsForm
- Form2Content
- ImageRecycle
- Regular Labs Snippets
- PWT ACL
- Speed Cache
- AdminTools
- AkeebaBackup
- JSitemap
---